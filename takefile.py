from bs4 import BeautifulSoup
import urllib3

filename = input('filename:')

http = urllib3.PoolManager()

url = f'http://127.0.0.1:8000/display_{filename}'
response = http.request('GET', url)
soup = BeautifulSoup(response.data, "html.parser")

with open(f'{filename}.json', 'wb') as f:
    f.write(soup.encode('utf-8'))
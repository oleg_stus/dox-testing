from django.http import JsonResponse
from django.shortcuts import render
import json

from .models import *


def example(request):
    try:
        data = json.loads(request.body.decode())
    except ValueError:
        return JsonResponse({
            'error': 'bla bla bla',
        })

def index(request):
    return render(request, 'index.html')


def display_all(request):
    items = FilesDataOx.objects.all()
    context = {
        'items': items,
        'header': 'All'
    }


    return render(request, 'index.html', context)


def display_cldr(request):
    items = CLDR.objects.all()
    context = {
        'items': items,
        'header': 'CLDR'
    }

    return render(request, 'index.html', context)


def display_docu(request):
    items = DOCU.objects.all()
    context = {
        'items': items,
        'header': 'DOCU'
    }

    return render(request, 'index.html', context)


def display_pd(request):
    items = PD.objects.all()
    context = {
        'items': items,
        'header': 'PD'
    }

    return render(request, 'index.html', context)


def display_pins(request):
    items = PINS.objects.all()
    context = {
        'items': items,
        'header': 'PINS'
    }

    return render(request, 'index.html', context)


def display_pvl(request):
    items = PVL.objects.all()
    context = {
        'items': items,
        'header': 'PVL'
    }


    return render(request, 'index.html', context)


def display_run(request):
    items = RUN.objects.all()
    context = {
        'items': items,
        'header': 'RUN'
    }

    return render(request, 'index.html', context)


def display_zm(request):
    items = ZM.objects.all()
    context = {
        'items': items,
        'header': 'ZM'
    }

    return render(request, 'index.html', context)


def display_zuo(request):
    items = ZUO.objects.all()
    context = {
        'items': items,
        'header': 'ZUO'
    }

    return render(request, 'index.html', context)



from django.conf.urls import url
from .views import *

urlpatterns = [

    url(r'^$', index, name='index'),
    url(r'^display_all$', display_all, name='display_all'),
    url(r'^display_cldr$', display_cldr, name='display_cldr'),
    url(r'^display_docu$', display_docu, name='display_docu'),
    url(r'^display_pd$', display_pd, name='display_pd'),
    url(r'^display_pins$', display_pins, name='display_pins'),
    url(r'^display_pvl$', display_pvl, name='display_pvl'),
    url(r'^display_run$', display_run, name='display_run'),
    url(r'^display_zm$', display_zm, name='display_zm'),
    url(r'^display_zuo$', display_zuo, name='display_zuo'),
]
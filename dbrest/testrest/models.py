from django.db import models

class FilesDataOx(models.Model):
    Date = models.TextField(max_length=255, verbose_name='date', default=0)
    Open = models.CharField(max_length=255, verbose_name='open', default=0)
    High = models.CharField(max_length=255, verbose_name='high', default=0)
    Low = models.CharField(max_length=255, verbose_name='low', default=0)
    Close = models.CharField(max_length=255, verbose_name='close', default=0)
    Adj_close = models.CharField(max_length=255, verbose_name='adj_close', default=0)
    Volume = models.CharField(max_length=255, verbose_name='volume', default=0)



    def __str__(self):
        return 'Date : {0} Volume : {1}'.format(self.Date, self.Volume)



class CLDR(FilesDataOx):
    pass

class DOCU(FilesDataOx):
    pass

class PD(FilesDataOx):
    pass

class PINS(FilesDataOx):
    pass

class PVL(FilesDataOx):
    pass

class RUN(FilesDataOx):
    pass

class ZM(FilesDataOx):
    pass

class ZUO(FilesDataOx):
    pass
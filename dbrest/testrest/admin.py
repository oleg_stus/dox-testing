from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin


@admin.register(FilesDataOx, CLDR, DOCU, PD, PVL, PINS, RUN, ZM, ZUO)
class ViewAdmin(ImportExportModelAdmin):
    pass